<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\SendEnterTainLinesMail;

class ChuckLines extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send-a-joke {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get a Chuck Norris joke by e-mail';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * @param string $email The e-mail address the que is sending the joke
     * @return int
   **/
    public function handle($email=null)
    {
        if($email === null) {
          $email = config('app.email_to')[0];
        }
        SendEnterTainLinesMail::dispatch($email)->onQueue('emails');
        $this->info("An e-mail is set to send! Please check your inbox. ");
        $this->info("If you did not receive an e-mail, than you should contact your ");
        $this->info("system administrator to check!\n");
        exit;
    }
}
