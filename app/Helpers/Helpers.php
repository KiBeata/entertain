<?php


namespace App\Helpers;
use App\EntertainmentEntities;
use App\EntertainmentSource;
use Illuminate\Support\Facades\Http;


class Helpers {

  /**
  * The getter funtion of the Chuck Norris source
  *
  *  @return string
  **/
  private function chuckNorrisRegUrl() {
    if(EntertainmentSource::where('name','Chuck Norris')->doesntExist()) {
      $data = ['name'=>'Chuck Norris', 'url'=>'https://api.chucknorris.io/jokes/random'];
      EntertainmentSource::insert($data);
    }
    return EntertainmentSource::where('name','Chuck Norris')->pluck('url')->first();
  }

  /**
  * The getter funtion of the Chuck Norris source
  *
  *  @param string $towho The e-mail address to who the joke is sent to
  *
  *  @return string
  **/
  public function sendEnterTainLines($towho) {
    $eurl = $this->chuckNorrisRegUrl();
    $response = Http::get($eurl);
    if($response->ok()) {
      $resp = $response->json();
      if(EntertainmentEntities::where('funvalue',$resp)->doesntExist()) {
        $data = [
          'funvalue' => json_encode($resp),
          'entertainment_sources_id' => EntertainmentSource::where('url', $eurl)->pluck('id')->first()
        ];
        EntertainmentEntities::insert($data);
      }
      $from = config('app.email_from')[0];
      return $this->sendMail($from,$towho,$resp['value'], $resp['icon_url']);
    }

  }

  /**
  * The private sending process
  *
  * @param string $from The e-mail address of the sender
  * @param string $to The e-mail address of the receiver
  * @param string $text The e-mail body text part
  * @param string $icon The e-mail body icon picture url
  *
  * @return string
  **/
  protected function sendMail($from, $to, $text, $icon=null) {
    $headers = 'From: '. $from . "\r\n" .
                'Reply-To: ' . $from . "\r\n" .
                'X-Mailer: PHP/' . phpversion();
    try {
      $content = "<img src='" . $icon . "' alt='jokeimg' title='jokeimg'/>" . "\n" . $text;
      $subject = 'Enter Tain Message';
      if(mail($to,$subject,$content, $headers)) {
        return 'Success!';
      } else {
        throw new \Exception(error_get_last()['message'], 1);
      }

    } catch(Exception $e) {
      return $e->getMessage();
    }
  }


}
 ?>
