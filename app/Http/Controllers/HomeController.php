<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Jobs\SendEnterTainLinesMail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth');
    }
  
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      return view('home');
    }

    /**
    * Sends an email from the API to a given email address
    *
    * @return void
    **/

    public function sendMeAJoke() {
      SendEnterTainLinesMail::dispatch(config('app.email_to')[0])
                              ->onQueue('emails');
    }
}
