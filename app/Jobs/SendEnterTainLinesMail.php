<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Helpers\Helpers;

class SendEnterTainLinesMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    private $towho;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($towho)
    {
      $this->towho = $towho;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $help = new Helpers();
      return $help->sendEnterTainLines($this->towho);
    }
}
